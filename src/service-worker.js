import { MOVENET_CONFIGS, LINE_WIDTH, DEFAULT_RADIUS } from "./config"

self.onmessage = async function (e) {
    console.log("in worker", e.data)
    const { poseDetector, video } = e.data;
    const poses = await poseDetector.estimatePoses(video, {
        maxPoses: MOVENET_CONFIGS.maxPoses, //When maxPoses = 1, a single pose is detected
        flipHorizontal: false
    });
    self.postMessage(poses);
};