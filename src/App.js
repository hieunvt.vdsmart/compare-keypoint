import * as tfJsCore from '@tensorflow/tfjs-core';
import * as tfJsConverter from '@tensorflow/tfjs-converter';
import * as tfJsBackendWebgl from '@tensorflow/tfjs-backend-webgl';
import { useEffect, useRef, useState } from "react";
import Webcam from "react-webcam";
import { isMobile } from 'react-device-detect';
import * as poseDetection from '@tensorflow-models/pose-detection';
import { MOVENET_CONFIGS, LINE_WIDTH, DEFAULT_RADIUS } from "./config"
import * as ml5 from "ml5";
import modeljson from "./models/model.json";
import metadata from "./models/model_meta.json";
import asanas from "./models/asanas.json";
import './App.css'
import { useLocation } from "react-router-dom";
import SockJS from 'sockjs-client';

const AUDIO_ACTION = {
    DOWN_RIGHT_LEG: 'ban_hay_ha_thap_chan_phai_xuong.wav',
    DOWN_LEFT_LEG: 'ban_hay_ha_thap_chan_trai_xuong.wav',
    DOWN_RIGHT_HAND: 'ban_hay_ha_thap_tay_phai_xuong.wav',
    DOWN_LEFT_HAND: 'ban_hay_ha_thap_tay_trai_xuong.wav',
    UP_RIGHT_LEG: 'ban_hay_nang_chan_phai_len.wav',
    UP_LEFT_LEG: 'ban_hay_nang_chan_trai_len.wav',
    UP_RIGHT_HAND: 'ban_hay_nang_tay_phai_len.wav',
    UP_LEFT_HAND: 'ban_hay_nang_tay_trai_len.wav',

    INCORRECT_RIGHT_LEG: 'audio/dong_tac_chan_phai_chua_dung_190923113958.wav',
    INCORRECT_LEFT_LEG: 'audio/dong_tac_chan_trai_chua_dung_190923113944.wav',
    INCORRECT_NECK: 'audio/dong_tac_co_chua_dung_190923114130.wav',
    INCORRECT_RIGHT_HIP: 'audio/dong_tac_hong_phai_chua_dung_190923114024.wav',
    INCORRECT_LEFT_HIP: 'audio/dong_tac_hong_trai_chua_dung_190923114040.wav',
    INCORRECT_RIGHT_HAND: 'audio/dong_tac_tay_phai_chua_dung_190923105243.wav',
    INCORRECT_LEFT_HAND: 'audio/dong_tac_tay_trai_chua_dung_190923113928.wav',
    INCORRECT_RIGHT_SHOULDER: 'audio/dong_tac_vai_phai_chua_dung_190923114114.wav',
    INCORRECT_LEFT_SHOULDER: 'audio/dong_tac_vai_trai_chua_dung_190923114058.wav',
}

let detector;
let start_inference_time;
let num_of_inferences = 0;
let total_inference_time = 0;
let last_panel_update = 0;
let rafId;
let canvas;
let contex2d;
let model;
let modelType;
let brain;
let poseLabel = "pose";
let text = "oki";
let poseScore = "0";
let lastPoseLabel;

const options = {
    task: 'classification' // or 'regression'
}
const modelDetails = {
    model: modeljson,
    metadata: metadata,
    weights: "https://cdn.glitch.global/e6659bd5-94b1-4dbc-94f9-5468bd8f317d/model.weights.bin?v=1642380976991"
}

// this loads the ml5 Neural Network model with the options specified and the files uploaded
brain = ml5.neuralNetwork(options);
console.log(modelDetails)

brain.load(modelDetails, modelLoaded)

function modelLoaded() {
    // continue on your neural network journey
    // use nn.classify() for classifications or nn.predict() for regressions
}

console.log("after loading the model", brain)

const VIDEO_CONFIGS = {
    facingMode: "user",
    deviceId: "",
    frameRate: { max: 60, ideal: 30 },
    width: isMobile ? 360 : 640,
    height: isMobile ? 270 : 480
};

let canvas2;
let contex2;

function App() {
    const [cameraReady, setCameraReady] = useState(false);
    const [displayFps, setDisplayFps] = useState(0);
    //socket
    const [socket, setSocket] = useState(null);

    const webcamRef = useRef({});
    const videoRef = useRef({});

    const search = useLocation().search;
    const videoQueryUrl = new URLSearchParams(search).get('videoUrl');
    const deviceQueryId = new URLSearchParams(search).get('deviceId');

    const [webcamPoses, setWebcamPoses] = useState([]);
    const [videoPoses, setVideoPoses] = useState([]);

    // const webcamWorker = new Worker("service-worker.js");

    //socket
    useEffect(() => {
        // Create a WebSocket connection
        const ws = new SockJS('https://admin.visedu.vn:10085/websocket');

        ws.onopen = () => {
            console.log('WebSocket connection opened.');
            setSocket(ws);
        };

        ws.onmessage = (event) => {
            console.log(event.data)
            handlePlayAudio(event.data);
        };

        ws.onclose = () => {
            console.log('WebSocket connection closed.');
        };

        return () => {
            // Clean up WebSocket connection on component unmount
            if (socket) {
                socket.close();
            }
        };
    }, []);

    useEffect(() => {
        _loadPoseNet().then();
        // const video = videoRef.current && videoRef.current;
        // webcamWorker.postMessage("xxx");

    }, []);

    //const [videoUrl, setVideoUrl] = useState("https://admin.visedu.vn:10081/book/41f5f966-ce66-4d16-9bbf-ef4045c4a02f.mp4");
    const [videoUrl, setVideoUrl] = useState(videoQueryUrl);
    const [deviceId, setDeviceId] = useState(deviceQueryId);

    useEffect(() => {
        const queryParams = new URLSearchParams(window.location.search);
        const videoParam = queryParams.get('video');

        //console.log(videoParam);

        if (videoParam) {
            setVideoUrl(videoParam);
        }
    }, []);

    const _loadPoseNet = async () => {
        if (rafId) {
            window.cancelAnimationFrame(rafId);
            detector.dispose();
        }

        detector = await createDetector();
        await renderPrediction();
    }

    const createDetector = async () => {
        model = poseDetection.SupportedModels.MoveNet;
        modelType = poseDetection.movenet.modelType.SINGLEPOSE_LIGHTNING; //or SINGLEPOSE_THUNDER
        return await poseDetection.createDetector(model, { modelType: modelType });
    }

    const renderPrediction = async () => {
        await renderResult();
        rafId = requestAnimationFrame(renderPrediction);
    }

    const renderResult = async () => {
        //console.log(webcamRef);
        //console.log(videoRef);

        const webcam = webcamRef.current && webcamRef.current['video'];

        const video = videoRef.current && videoRef.current;

        //video.load();

        if (!cameraReady && !webcam) {
            return;
        }

        if (webcam.readyState < 2) {
            return;
        }

        if (!video) {
            return;
        }


        if (video?.readyState < 2) {
            return;
        }
        video.play();

        //console.log(webcam);
        //console.log(video);

        startEstimatePoses();

        const webcamPoses = await detector.estimatePoses(webcam, {
            maxPoses: MOVENET_CONFIGS.maxPoses, //When maxPoses = 1, a single pose is detected
            flipHorizontal: false
        });

        const videoPoses = await detector.estimatePoses(video, {
            maxPoses: MOVENET_CONFIGS.maxPoses, //When maxPoses = 1, a single pose is detected
            flipHorizontal: false
        });
        
        stopEstimatePoses();
        getContexFullScreen(webcam);
        getVideoContexFullScreen(video);

        let score = {};

        if (videoPoses.length > 0 && webcamPoses.length > 0) {
            score = comparePose(webcamPoses[0]["keypoints"], videoPoses[0]["keypoints"], webcam, video);
            //console.log(score);
            //score > 0.5 ? contex2d.strokeStyle = '#04d616' : contex2d.strokeStyle = '#ff0303';
            //setScoreJoin(score);
        }

        // console.log(score);

        if (webcamPoses.length > 0) {
            //console.log("========", webcamPoses);
            getResultsFullScreen(webcamPoses, score);
        }

        if (videoPoses.length > 0) {
            //console.log("========", videoPoses);
            getResultsVideoFullScreen(videoPoses);
        }

    }

    const [scoreJoin, setScoreJoin] = useState(0);

    const startEstimatePoses = () => {
        start_inference_time = (performance || Date).now();
    }

    const stopEstimatePoses = () => {
        const endInferenceTime = (performance || Date).now();
        total_inference_time += endInferenceTime - start_inference_time;
        ++num_of_inferences;
        const panelUpdateMilliseconds = 1000;

        if (endInferenceTime - last_panel_update >= panelUpdateMilliseconds) {
            const averageInferenceTime = total_inference_time / num_of_inferences;
            total_inference_time = 0;
            num_of_inferences = 0;
            setDisplayFps(1000.0 / averageInferenceTime, 120);
            last_panel_update = endInferenceTime;
        }
    }

    const webcamRatio = 0.7;
    const videoRatio = 0.45;

    const glScoreThreshold = 0.8

    const getContexFullScreen = (video) => {
        canvas = document.getElementById('canvas');
        contex2d = canvas.getContext('2d');

        const videoWidth = video.videoWidth;
        const videoHeight = video.videoHeight;

        video.width = videoWidth;
        video.height = videoHeight;

        canvas.width = videoWidth * webcamRatio;
        canvas.height = videoHeight * webcamRatio;
        // canvas.height = 203;
        // canvas.width = videoWidth * (203 / videoHeight);
        contex2d.fillRect(0, 0, videoWidth, videoHeight);

        contex2d.translate(video.videoWidth * webcamRatio, 0);
        contex2d.scale(-webcamRatio, webcamRatio);
        contex2d.drawImage(video, 0, 0, videoWidth, videoHeight);
    }

    const getResultsFullScreen = (poses, score) => {
        for (const pose of poses) {
            //getResults(pose);
            if (pose?.keypoints != undefined && pose?.keypoints != null) {
                let inputs = [];
                for (let i = 0; i < pose.keypoints.length; i++) {
                    let x = pose.keypoints[i].x;
                    let y = pose.keypoints[i].y;
                    inputs.push(x);
                    inputs.push(y);
                }
                getKeyPoints(pose.keypoints, score);
                drawSkeleton(pose.keypoints, score);
                // brain.classify(inputs, handleclassify);
                // console.log("classify", brain)
            }
        }
    }

    const getResults = (pose) => {
        if (pose?.keypoints != undefined && pose?.keypoints != null) {
            let inputs = [];
            for (let i = 0; i < pose.keypoints.length; i++) {
                let x = pose.keypoints[i].x;
                let y = pose.keypoints[i].y;
                inputs.push(x);
                inputs.push(y);
            }
            getKeyPoints(pose.keypoints);
            drawSkeleton(pose.keypoints);
            // brain.classify(inputs, handleclassify);
            // console.log("classify", brain)
        }

    }

    function handleclassify(error, results) {
        //console.log("resulst",results)
        if (results && results[0].confidence > 0.70) {
            poseLabel = results[0].label;
            poseScore = results[0].confidence.toFixed(2);

            // this tells it to run the writePose & writeInfo functions
            if (lastPoseLabel !== poseLabel) {
                console.log("we got to this point")
            }
            lastPoseLabel = results[0].label;
        }

        // here it calls for classifyPose again with a timeout

        setTimeout(getResults, 1000);
    }

    function handleResults(error, result) {
        if (error) {
            console.error(error);
            return;
        }
        console.log(result); // {label: 'red', confidence: 0.8};
    }



    const getKeyPoints = (keypoints, score) => {

        const keypointInd = poseDetection.util.getKeypointIndexBySide(model);
        contex2d.fillStyle = '#fff';
        contex2d.strokeStyle = '#fff';
        contex2d.lineWidth = LINE_WIDTH;

        for (const i of keypointInd.middle) {
            getKeyPoint(keypoints[i], score);
        }

        //contex2d.fillStyle = '#00ff00';

        for (const i of keypointInd.left) {
            getKeyPoint(keypoints[i], score);
        }

        //contex2d.fillStyle = '#ffff00';

        for (const i of keypointInd.right) {
            getKeyPoint(keypoints[i], score);
        }
    }

    const getKeyPoint = (keypoint, glScore) => {
        // If score is null, just show the keypoint.
        const score = keypoint.score != null ? keypoint.score : 1;
        const scoreThreshold = MOVENET_CONFIGS.scoreThreshold || 0;

        if (score >= scoreThreshold) {
            const circle = new Path2D();
            circle.arc(keypoint.x, keypoint.y, 4, 0, 2 * Math.PI);
            contex2d.fill(circle);
            contex2d.stroke(circle);
        }
    }

    const drawSkeleton = (keypoints, score) => {
        contex2d.fillStyle = 'White';
        contex2d.strokeStyle = score.numError <= 5 ? '#24ba06' : '#cc160c';
        //contex2d.strokeStyle = '#24ba06';
        contex2d.lineWidth = LINE_WIDTH;
        poseDetection.util.getAdjacentPairs(model).forEach(([i, j]) => {
            //contex2d.strokeStyle = '#24ba06';
            const keypoint1 = keypoints[i];
            const keypoint2 = keypoints[j]; // If score is null, just show the keypoint.

            const score1 = keypoint1.score != null ? keypoint1.score : 1;
            const score2 = keypoint2.score != null ? keypoint2.score : 1;
            const scoreThreshold = MOVENET_CONFIGS.scoreThreshold || 0;

            if (score.numError > 0 && checkPairInArray([i, j], score.array))
                contex2d.strokeStyle = '#cc160c';

            if (score1 >= scoreThreshold && score2 >= scoreThreshold) {
                contex2d.beginPath();
                contex2d.moveTo(keypoint1.x, keypoint1.y);
                contex2d.lineTo(keypoint2.x, keypoint2.y);
                contex2d.stroke();
            }

            if (score.numError <= 5) contex2d.strokeStyle = '#24ba06';
        });
    }

    const checkPairInArray = (pair, array) => {
        return array.includes(pair[0]) && array.includes(pair[1]);
    }

    const onUserMediaError = () => {
        console.log('ERROR Occured');
    };

    const onUserMedia = () => {
        console.log('Camera loaded!');
        setCameraReady(true);
    };

    // video skeleton

    const getVideoContexFullScreen = (video) => {
        canvas2 = document.getElementById('canvas2');
        contex2 = canvas2.getContext('2d');

        const videoWidth = video.videoWidth;
        const videoHeight = video.videoHeight;

        video.width = videoWidth;
        video.height = videoHeight;

        canvas2.width = videoWidth * videoRatio;
        canvas2.height = videoHeight * videoRatio;
        contex2.fillRect(0, 0, videoWidth, videoHeight);

        contex2.translate(video.videoWidth * videoRatio, 0);
        contex2.scale(-videoRatio, videoRatio);
        contex2.drawImage(video, 0, 0, videoWidth, videoHeight);
    }

    const getResultsVideoFullScreen = (poses) => {
        for (const pose of poses) {
            if (pose?.keypoints != undefined && pose?.keypoints != null) {
                let inputs = [];
                for (let i = 0; i < pose.keypoints.length; i++) {
                    let x = pose.keypoints[i].x;
                    let y = pose.keypoints[i].y;
                    inputs.push(x);
                    inputs.push(y);
                }
                getVideoKeyPoints(pose.keypoints);
                drawVideoSkeleton(pose.keypoints);
                // brain.classify(inputs, handleclassify);
                // console.log("classify", brain)
            }
        }
    }

    const getVideoKeyPoints = (keypoints) => {
        const keypointInd = poseDetection.util.getKeypointIndexBySide(model);
        contex2.fillStyle = '#fff';
        contex2.strokeStyle = '#fff';
        contex2.lineWidth = LINE_WIDTH;

        for (const i of keypointInd.middle) {
            getVideoKeyPoint(keypoints[i]);
        }

        //contex2.fillStyle = '#00ff00';

        for (const i of keypointInd.left) {
            getVideoKeyPoint(keypoints[i]);
        }

        //contex2.fillStyle = '#ffff00';

        for (const i of keypointInd.right) {
            getVideoKeyPoint(keypoints[i]);
        }
    }

    const getVideoKeyPoint = (keypoint) => {
        // If score is null, just show the keypoint.
        const score = keypoint.score != null ? keypoint.score : 1;
        const scoreThreshold = MOVENET_CONFIGS.scoreThreshold || 0;

        if (score >= scoreThreshold) {
            const circle = new Path2D();
            circle.arc(keypoint.x, keypoint.y, DEFAULT_RADIUS, 0, 2 * Math.PI);
            contex2.fill(circle);
            contex2.stroke(circle);
        }
    }

    const drawVideoSkeleton = (keypoints) => {
        contex2.fillStyle = 'White';
        contex2.strokeStyle = 'White';
        contex2.lineWidth = LINE_WIDTH;
        poseDetection.util.getAdjacentPairs(model).forEach(([i, j]) => {
            const keypoint1 = keypoints[i];
            const keypoint2 = keypoints[j]; // If score is null, just show the keypoint.

            const score1 = keypoint1.score != null ? keypoint1.score : 1;
            const score2 = keypoint2.score != null ? keypoint2.score : 1;
            const scoreThreshold = MOVENET_CONFIGS.scoreThreshold || 0;

            if (score1 >= scoreThreshold && score2 >= scoreThreshold) {
                contex2.beginPath();
                contex2.moveTo(keypoint1.x, keypoint1.y);
                contex2.lineTo(keypoint2.x, keypoint2.y);
                contex2.stroke();
            }
        });
    }

    //compare pose
    const sigmas = [0.026, 0.025, 0.025, 0.035, 0.035, 0.079, 0.079, 0.072, 0.072, 0.062, 0.062, 0.107, 0.107, 0.087, 0.087, 0.089, 0.089];
    /* const k = 3;
    const comparePose = (webcamPoses, videoPoses, webcam, video) => {
        let total_oks = 0;
        let count = 0;
        for (let i = 0; i < 17; i++) {
            let { y: y1, x: x1, score: v1 } = webcamPoses[i];
            let { y: y2, x: x2, score: v2 } = videoPoses[i];

            let sigma = sigmas[i];

            x1 /= webcam.videoWidth;
            y1 /= webcam.videoHeight;

            x2 /= video.videoWidth;
            y2 /= video.videoHeight;

            //console.log(x1,x2);

            if (v2 > 0) {
                let d = (y1 - y2) ** 2 + (x1 - x2) ** 2;
                if (v1 > 0.5) {
                    total_oks += Math.exp((-d) / (2 * k * k * sigma * sigma));
                    count += 1;
                }

            }
        }
        return !count ? 0 : total_oks / count;
    } */

    const scaleFator = (webcamPoses, videoPoses) => {
        let videoMaxX = Math.max(...videoPoses.map(e => e.x));
        let videoMinX = Math.min(...videoPoses.map(e => e.x));
        let videoMaxY = Math.max(...videoPoses.map(e => e.y));
        let videoMinY = Math.min(...videoPoses.map(e => e.y));
        let videoArea = (videoMaxX - videoMinX) * (videoMaxY - videoMinY);

        //console.log(videoMaxX, videoMinX);

        let webcamMaxX = Math.max(...webcamPoses.map(e => e.x));
        let webcamMinX = Math.min(...webcamPoses.map(e => e.x));
        let webcamMaxY = Math.max(...webcamPoses.map(e => e.y));
        let webcamMinY = Math.min(...webcamPoses.map(e => e.y));
        let webcamArea = (webcamMaxX - webcamMinX) * (webcamMaxY - webcamMinY);

        return videoArea / (webcamArea + 0.0000001);
    }

    const comparePose = (webcamPoses, videoPoses, webcam, video) => {
        let scaleFactor = scaleFator(webcamPoses, videoPoses);

        let leftHand = comparePartOfPose([webcamPoses[5], webcamPoses[7], webcamPoses[9]], [videoPoses[5], videoPoses[7], videoPoses[9]], webcam, video, [sigmas[5], sigmas[7], sigmas[9]], scaleFactor);
        let rightHand = comparePartOfPose([webcamPoses[6], webcamPoses[8], webcamPoses[10]], [videoPoses[6], videoPoses[8], videoPoses[10]], webcam, video, [sigmas[6], sigmas[8], sigmas[10]], scaleFactor);
        let leftShoulder = comparePartOfPose([webcamPoses[7], webcamPoses[5], webcamPoses[11]], [videoPoses[7], videoPoses[5], videoPoses[11]], webcam, video, [sigmas[7], sigmas[5], sigmas[11]], scaleFactor);
        let rightShoulder = comparePartOfPose([webcamPoses[8], webcamPoses[6], webcamPoses[12]], [videoPoses[8], videoPoses[6], videoPoses[12]], webcam, video, [sigmas[8], sigmas[6], sigmas[12]], scaleFactor);
        let leftHip = comparePartOfPose([webcamPoses[5], webcamPoses[11], webcamPoses[13]], [videoPoses[5], videoPoses[11], videoPoses[13]], webcam, video, [sigmas[5], sigmas[11], sigmas[13]], scaleFactor);
        let rightHip = comparePartOfPose([webcamPoses[6], webcamPoses[12], webcamPoses[14]], [videoPoses[6], videoPoses[12], videoPoses[14]], webcam, video, [sigmas[6], sigmas[12], sigmas[14]], scaleFactor);
        let leftLeg = comparePartOfPose([webcamPoses[11], webcamPoses[13], webcamPoses[15]], [videoPoses[11], videoPoses[13], videoPoses[15]], webcam, video, [sigmas[11], sigmas[13], sigmas[15]], scaleFactor);
        let rightLeg = comparePartOfPose([webcamPoses[12], webcamPoses[14], webcamPoses[16]], [videoPoses[12], videoPoses[14], videoPoses[16]], webcam, video, [sigmas[12], sigmas[14], sigmas[16]], scaleFactor);
        let neck = comparePartOfPose([webcamPoses[6], webcamPoses[0], webcamPoses[5]], [videoPoses[6], videoPoses[0], videoPoses[5]], webcam, video, [sigmas[6], sigmas[0], sigmas[5]], scaleFactor);

        let array = [
            { value: leftHand, array: [5, 7, 9] },
            { value: rightHand, array: [6, 8, 10] },
            { value: leftShoulder, array: [7, 5, 11] },
            { value: rightShoulder, array: [8, 6, 12] },
            { value: leftHip, array: [5, 11, 13] },
            { value: rightHip, array: [6, 12, 14] },
            { value: leftLeg, array: [11, 13, 15] },
            { value: rightLeg, array: [12, 14, 16] },
            // { value: neck, array: [6, 0, 5]},
        ];

        //let minValue = Math.min(...array.map(e => e.value));
        let numError = array.filter(e => e.value);
        let result = {};
        if (numError.length) {
            result = numError[0];
        }
        result.numError = numError.length;
        return result;

    }
    const comparePartOfPoseOld = (webcamPoses, videoPoses, webcam, video, innerSigmas, scaleFactor) => {
        let total_oks = 0;
        let count = 0;
        for (let i = 0; i < 3; i++) {
            let { y: y1, x: x1, score: v1 } = webcamPoses[i];
            let { y: y2, x: x2, score: v2 } = videoPoses[i];

            let sigma = 2 * innerSigmas[i];

            x1 /= webcam.videoWidth;
            y1 /= webcam.videoHeight;

            x2 /= video.videoWidth;
            y2 /= video.videoHeight;

            if (v2 > 0) {
                let d = (y1 - y2) ** 2 + (x1 - x2) ** 2;
                if (v1 > 0) {
                    total_oks += Math.exp((-d) / (2 * scaleFactor * scaleFactor * sigma * sigma));
                    count += 1;
                }
            }
        }
        return !count ? 0 : total_oks / count;
    }

    const comparePartOfPose = (webcamPoses, videoPoses, webcam, video, innerSigmas, scaleFactor) => {
        let v1 = [webcamPoses[1].x - webcamPoses[0].x, webcamPoses[1].y - webcamPoses[0].y];
        let v2 = [webcamPoses[1].x - webcamPoses[2].x, webcamPoses[1].y - webcamPoses[2].y];

        let alphaWebcam = Math.acos((v1[0] * v2[0] + v1[1] * v2[1]) / (Math.sqrt(v1[0] * v1[0] + v1[1] * v1[1]) * Math.sqrt(v2[0] * v2[0] + v2[1] * v2[1]) + 0.0000001));

        v1 = [videoPoses[1].x - videoPoses[0].x, videoPoses[1].y - videoPoses[0].y];
        v2 = [videoPoses[1].x - videoPoses[2].x, videoPoses[1].y - videoPoses[2].y];

        let alphaVideo = Math.acos((v1[0] * v2[0] + v1[1] * v2[1]) / (Math.sqrt(v1[0] * v1[0] + v1[1] * v1[1]) * Math.sqrt(v2[0] * v2[0] + v2[1] * v2[1]) + 0.0000001));

        return Math.abs(alphaVideo - alphaWebcam) > Math.max(0.2 * alphaWebcam, 1 / 10);

    }

    const handlePlayAudio = (audioAction) => {
        const fileNameAudio = AUDIO_ACTION[audioAction]
        console.log(fileNameAudio)
        const audioPath = `/${fileNameAudio}`;
        const audio = new Audio(audioPath);
        audio.play();
    }

    return (
        <section >
            <div style={{ width: '100%', height: '100%' }}>
                <Webcam
                    style={{ display: "none" }}
                    ref={webcamRef}
                    audio={false}
                    height={isMobile ? 270 : 480}
                    width={isMobile ? 360 : 640}
                    videoConstraints={VIDEO_CONFIGS}
                    onUserMediaError={onUserMediaError}
                    onUserMedia={onUserMedia}
                />
                <div class="header">
                    {/* <div class="logo">Khoẻ 360</div> */}
                    <div className='container-logo'>
                        <div class="logo-holder logo-3">
                            <a href="">
                                <h3>KHOẺ 360</h3>
                                {/* <p>Yoga</p> */}
                            </a>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: 'auto' }}>
                        <div class="logo-image-vdsmart">
                            <img src="https://itviec.com/rails/active_storage/representations/proxy/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMUwxR0E9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--84616ba883eacfba8cc99b8a2b9b3a720f635cff/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJU2xCSEJqb0dSVlE2RW5KbGMybDZaVjkwYjE5bWFYUmJCMmtCcWpBPSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--328a861f9298cf336f3156564a69f746e4a952ab/LogoVDSMART.JPG" alt="SHIVOM YOGA" />
                        </div>
                        <div class="logo-image-shivom">
                            <img src="https://scontent.fhan3-1.fna.fbcdn.net/v/t39.30808-6/288386787_5414132618639491_6697448309058998779_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=a2f6c7&_nc_ohc=oOlyM2vqRt8AX_EcSfP&_nc_oc=AQk-EPBnajh9NjEooQ8aqkoS51L1eaNpQ079Gbgl7zISXN5a0znmuFO4X7CT77eA78Y&_nc_ht=scontent.fhan3-1.fna&oh=00_AfD9uVhNEUamPYxlf2GLz3eCz32C9MoXNlN-l7ZJlao1mw&oe=650DC609" alt="SHIVOM YOGA" />
                        </div>
                    </div>

                </div>
                {/* <div className='container-title'>
                    <div className='text-title'>LIVE CAMERA</div>
                    <div className='text-title'>VIDEO MẪU</div>
                </div> */}
                <div className='cam-video-container'>
                    <div className='canvas-container' style={{ position: 'relative' }}>
                        <div className='text-title'>LIVE CAMERA</div>
                        {/* height: 'auto'  */}
                        <canvas style={{ height: '90vh', width: '100%', objectFit: 'cover' }} id="canvas" />
                    </div>
                    <div className='canvas-container' style={{ position: 'relative' }}>
                        <div className='text-title'>VIDEO MẪU</div>
                        <canvas style={{ width: '100%', height: '90vh', objectFit: 'cover' }} id="canvas2" />
                    </div>
                </div>
                <video
                    style={{ position: "absolute", left: "-9999px", width: 0, height: 0 }}
                    className="hidden-video"
                    controls
                    autoPlay
                    playsInline
                    width="640"
                    ref={videoRef}
                    crossOrigin='anonymous'>
                    <source src={videoUrl} type="video/mp4" />
                </video>
            </div>
        </section>
    );
}

export default App;
