// webcam-worker.js
self.onmessage = async function (e) {
    const { poseDetector, video } = e.data;
    const poses = await poseDetector.estimatePoses(video, {
        maxPoses: MOVENET_CONFIGS.maxPoses, //When maxPoses = 1, a single pose is detected
        flipHorizontal: false
    });
    self.postMessage(poses);
};